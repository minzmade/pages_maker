#!/bin/bash
set -e

pandoc --columns=99999 --toc --template template.html README.md -o index.html
