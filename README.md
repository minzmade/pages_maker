---
title: Minzmade Codeberg Repositories Content Overview
---

---

<img align="right" style="max-width: 33%;" src="img/minzmade.png" />

Hi!

this page summarises some content from my Codeberg Repositories in a user-friendly way.

## Links
- [all Repositories with the source code](https://codeberg.org/minzmade)
- <a rel="me" href="https://chaos.social/@minzmade">find me on Mastodon</a>

## Presentations

### Shorties

<div class="indent">

#### Inkscape Overview
- small presentation about [Inkscape](https://inkscape.org) basics
- [presentation, code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/short_inkscape)

#### PKE - public key encryption
- general explaination about encrypting things with two keys
- <a href="presentations/pke.svg" target="_blank">**Start Presentation (SVG)**</a>
- [presentation, code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/short_pke)

</div>

### Passwords & Passphrases - how to handle Credentials?
- overview credential handling and security with simple explanations and attacking points with focus on passwords/passphrases and password manager
- <a href="presentations/passphrases.pdf" target="_blank">**Start Presentation (PDF)**</a>
- [presentation, code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/passphrases)

### Data Protection
- simple basic introduction; focus on understanding the why
- <a href="presentations/data_protection.pdf" target="_blank">**Start Presentation (PDF)**</a>
- [source code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/data_protection)

### Docker Basics
- Docker - a simple introduction; focus on understanding, best practice, not coding
- <a href="presentations/docker_basics.pdf" target="_blank">**Start Presentation (PDF)**</a>
- <a href="presentations/docker_basics_slim.pdf" target="_blank">**Start Presentation - shorter version (PDF)**</a>
- [Examples, source code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/docker_basics)

### Design Projects to obtain machine-readable Data and obj_tables
- short overview of common questions wen starting new projects; short overview ob obj_tables functionality
- <a href="presentations/getting_data_with_obj_tables.svg" target="_blank">**Start Presentation (SVG)**</a>
- [Examples, Docker-Commands, Links, source code, license,…](https://codeberg.org/minzmade/presentations/src/branch/main/obj_tables)

