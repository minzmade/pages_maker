#!/bin/bash
set -eu

readonly PROGPATH="$(pwd)"
readonly TARGET="$(pwd)/../pages"
dir_pre="presentations"

echo "will write to: $TARGET"
sleep 2

# index.html
echo "processing index.html"
cd "$PROGPATH"
bash process_html.sh
mv index.html "$TARGET"

# images
echo "processing images"
cd "$PROGPATH"
cp favicon.png "$TARGET"
mkdir -p "$TARGET/img"
cp minzmade.png "$TARGET/img"

mkdir -p "$TARGET/$dir_pre"

echo "processing presentations"

echo "           pke…"
cp ../presentations/short_pke/pke.svg "$TARGET/$dir_pre"

echo "           obj_tables…"
cp ../presentations/obj_tables/getting_data_with_obj_tables.svg "$TARGET/$dir_pre"
cp -r ../presentations/obj_tables/img "$TARGET/$dir_pre"

echo "           docker_basics…"
cd ../presentations/docker_basics/
pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Serif" --slide-level 2 --number-section --toc README.md -o "$TARGET/$dir_pre/docker_basics.pdf"
pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Serif" --slide-level 2 --number-section --toc README_slim.md -o "$TARGET/$dir_pre/docker_basics_slim.pdf"
cd "$TARGET"

echo "           data_protection…"
cd ../presentations/data_protection
pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Sans" --slide-level 2 --number-section --toc README.md -o "$TARGET/$dir_pre/data_protection.pdf"
cd "$TARGET"

echo "           passphrases…"
cd ../presentations/passphrases
pandoc --pdf-engine=xelatex -t beamer -f markdown+emoji -V mainfont="DejaVu Sans" --slide-level 2 --number-section --toc README.md -o "$TARGET/$dir_pre/passphrases.pdf"
cd "$TARGET"

echo "DONE"
exit 0
